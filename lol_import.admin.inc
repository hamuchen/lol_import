<?php

function lol_import_admin() {
  $form = array();

  $form['settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('RIOT API settings'),
  );
  $form['settings']['lol_import_url_base'] = array(
    '#type' => 'textfield',
    '#title' => t('Base URL'),
    '#description' => t('Base URL of the RIOT API.'),
    '#default_value' => variable_get('lol_import_url_base', ''),
  );
  $form['settings']['lol_import_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('RIOT API key'),
    '#description' => t('Your personal RIOT API key.'),
    '#default_value' => variable_get('lol_import_api_key', ''),
  );
  $form['settings']['lol_import_region'] = array(
    '#type' => 'textfield',
    '#title' => t('Region'),
    '#description' => t('Region where to call the API. (e.g. na, euw, ...)'),
    '#default_value' => variable_get('lol_import_region', ''),
  );

  $form['static'] = array(
    '#type' => 'fieldset',
    '#title' => t('Static settings'),
  );
  $form['static']['lol_import_url_realm'] = array(
    '#type' => 'textfield',
    '#title' => t('Realm'),
    '#default_value' => variable_get('lol_import_url_realm', ''),
  );
  $form['static']['lol_import_url_champion'] = array(
    '#type' => 'textfield',
    '#title' => t('Champions'),
    '#default_value' => variable_get('lol_import_url_champion', ''),
  );
  $form['static']['lol_import_url_item'] = array(
    '#type' => 'textfield',
    '#title' => t('Items'),
    '#default_value' => variable_get('lol_import_url_item', ''),
  );
  $form['static']['lol_import_url_mastery'] = array(
    '#type' => 'textfield',
    '#title' => t('Champions'),
    '#default_value' => variable_get('lol_import_url_mastery', ''),
  );
  $form['static']['lol_import_url_rune'] = array(
    '#type' => 'textfield',
    '#title' => t('Champions'),
    '#default_value' => variable_get('lol_import_url_rune', ''),
  );
  $form['static']['lol_import_url_spell'] = array(
    '#type' => 'textfield',
    '#title' => t('Champions'),
    '#default_value' => variable_get('lol_import_url_spell', ''),
  );

  return system_settings_form($form);
}

function lol_import_admin_manual() {
  return drupal_get_form('lol_import_admin_manual_form');
}

function lol_import_admin_manual_form($form, &$form_state) {
  $form['data'] = array(
    '#type' => 'fieldset',
    '#title' => t('Import Static Data'),
  );
  $options = array(
    'lol_import_champions' => t('Champions'),
    'lol_import_items' => t('Items'),
    'lol_import_mastery' => t('Mastery'),
    'lol_import_runes' => t('Runes'),
    'lol_import_summoner_spells' => t('Summoner Spells'),
  );
  $form['data']['data'] = array(
    '#type' => 'radios',
    '#title' => t('Data Import'),
    '#description' => t('Select which data should be imported. Can take some time!'),
    '#required' => TRUE,
    '#options' => $options,
  );

  $form['actions'] = array(
    '#type' => 'actions'
  );
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Import'),
  );
  return $form;
}

function lol_import_admin_manual_form_submit($form, &$form_state) {
  $data = $form_state['values']['data'];
  $operations = array(
    array(
      $data,
      array(),
    )
  );
  $batch = array(
    'title' => t('Import League of Legens static data ...'),
    'operations' => $operations,
    'init_message' => t('Initialize LoL Import...'),
  );
  batch_set($batch);
}
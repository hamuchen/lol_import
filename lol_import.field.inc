<?php

################################################################################
## FIELD HELPER

/**
 * Sets the field value for this term.
 *
 * @param stdClass $term term the value is set
 * @param string $field_name field to set the value to
 * @param mixed $value the value to set
 */
function lol_import_field_set_value(&$term, $field_name, $value) {
  if (is_array($value)) {
    $value = implode('<br />', $value);
  }
  $term->{$field_name}[LANGUAGE_NONE][0]['value'] = $value;
}

/**
 * Adds a field value for this term.
 *
 * @param stdClass $term term the value is added
 * @param string $field_name field to add the value to
 * @param mixed $value the value to add
 */
function lol_import_field_add_value(&$term, $field_name, $value) {
  if (is_array($value)) {
    $value = implode('<br />', $value);
  }
  $term->{$field_name}[LANGUAGE_NONE][]['value'] = $value;
}

/**
 * Sets the field tid (term id) for this term.
 *
 * @param stdClass $term term the tid is set
 * @param string $field_name field to set the tid to
 * @param string|int $tid the tid to set
 */
function lol_import_field_set_tid(&$term, $field_name, $tid) {
  $term->{$field_name}[LANGUAGE_NONE][0]['tid'] = $tid;
}

/**
 * Adds a field tid for this term.
 *
 * @param stdClass $term term the value is added
 * @param string $field_name field to add the value to
 * @param string|int $tid the tid to add
 */
function lol_import_field_add_tid(&$term, $field_name, $tid) {
  $term->{$field_name}[LANGUAGE_NONE][]['tid'] = $tid;
}

/**
 * Populate the image filed for this term.
 *
 * @param stdClass $term term the image is set
 * @param string $field_name field to set the image to
 * @param string $url the image url to download the image
 */
function lol_import_field_set_image(&$term, $field_name, $url) {
  // download file to temp
  $image = file_get_contents($url);
  $path = file_unmanaged_save_data($image);

  // save file
  $file = new StdClass();
  $file->uri = $path;
  $file->filemime = file_get_mimetype($path);
  $file->status = 1;
  $file->display = 1;
  $file = file_copy($file, 'public://lol/import');
  $term->{$field_name}[LANGUAGE_NONE][0] = (array) $file;
}

################################################################################
## TEXT FIELD

/**
 * Create a field and field instance of a text field (if not exists).
 *
 * @param string $vocabulary_name machine readable name of the vocabulary
 * @param string $field_name machine readable name of the field
 * @param string $label field label
 * @param int $cardinality Number of values (-1 for unlimited)
 */
function lol_import_field_create_text($vocabulary_name, $field_name, $label,
                                      $cardinality = 1) {
  lol_import_field_create_text_field($field_name, $cardinality);
  lol_import_field_create_text_instance($vocabulary_name, $field_name, $label);
}

function lol_import_field_create_text_field($field_name, $cardinality = 1) {
  $field = array(
    'field_name' => $field_name,
    'type' => 'text',
    'entity_type' => 'taxonomy_term',
    'cardinality' => $cardinality,
  );
  lol_import_field_add_field($field);
}

function lol_import_field_create_text_instance($vocabulary_name, $field_name,
                                               $label) {
  $instance = array(
    'field_name' => $field_name,
    'entity_type' => 'taxonomy_term',
    'label' => $label,
    'bundle' => $vocabulary_name,
    'widget' => array(
      'type' => 'textfield',
    ),
  );
  lol_import_field_add_instance($instance);
}

################################################################################
## TEXT AREA

/**
 * Create a field and field instance of a textarea field (if not exists).
 *
 * @param string $vocabulary_name machine readable name of the vocabulary
 * @param string $field_name machine readable name of the field
 * @param string $label field label
 * @param int $cardinality Number of values (-1 for unlimited)
 */
function lol_import_field_create_area($vocabulary_name, $field_name, $label,
                                      $cardinality = 1) {
  lol_import_field_create_area_field($field_name, $cardinality);
  lol_import_field_create_area_instance($vocabulary_name, $field_name, $label);
}

function lol_import_field_create_area_field($field_name, $cardinality = 1) {
  $field = array(
    'field_name' => $field_name,
    'entity_type' => 'taxonomy_term',
    'type' => 'text_long',
    'module' => 'text',
    'cardinality' => $cardinality,
  );
  lol_import_field_add_field($field);
}

function lol_import_field_create_area_instance($vocabulary_name, $field_name,
                                               $label) {
  $instance = array(
    'field_name' => $field_name,
    'entity_type' => 'taxonomy_term',
    'label' => $label,
    'bundle' => $vocabulary_name,
    'widget' => array(
      'type' => 'text_textarea',
      'module' => 'text',
    ),
  );
  lol_import_field_add_instance($instance);
}

################################################################################
## IMAGE FIELD

/**
 * Create a field and field instance of an image field (if not exists).
 *
 * @param string $vocabulary_name machine readable name of the vocabulary
 * @param string $field_name machine readable name of the field
 * @param string $label field label
 * @param int $cardinality Number of values (-1 for unlimited)
 */
function lol_import_field_create_image($vocabulary_name, $field_name,
                                       $label, $cardinality = 1) {
  lol_import_field_create_image_field($field_name, $cardinality);
  lol_import_field_create_image_instance($vocabulary_name, $field_name, $label);
}

function lol_import_field_create_image_field($field_name, $cardinality = 1) {
  $field = array(
    'field_name' => $field_name,
    'type' => 'image',
    'entity_type' => 'taxonomy_term',
    'cardinality' => $cardinality,
  );
  lol_import_field_add_field($field);
}

function lol_import_field_create_image_instance($vocabulary_name, $field_name,
                                                $label) {
  $instance = array(
    'field_name' => $field_name,
    'entity_type' => 'taxonomy_term',
    'bundle' => $vocabulary_name,
    'label' => $label,
    'settings' => array(
      'file_directory' => 'lol/import',
    ),
  );
  lol_import_field_add_instance($instance);
}

################################################################################
## BOOLEAN FIELD

/**
 * Create a field and field instance of a boolean field (if not exists).
 *
 * @param string $vocabulary_name machine readable name of the vocabulary
 * @param string $field_name machine readable name of the field
 * @param string $label field label
 * @param int $cardinality Number of values (-1 for unlimited)
 */
function lol_import_field_create_boolean($vocabulary_name, $field_name,
                                         $label, $cardinality = 1) {
  lol_import_field_create_boolean_field($field_name, $cardinality);
  lol_import_field_create_boolean_instance($vocabulary_name, $field_name,
                                           $label);
}

function lol_import_field_create_boolean_field($field_name, $cardinality = 1) {
  $field = array(
    'field_name' => $field_name,
    'type' => 'list_boolean',
    'entity_type' => 'taxonomy_term',
    'cardinality' => $cardinality,
  );
  lol_import_field_add_field($field);
}

function lol_import_field_create_boolean_instance($vocabulary_name, $field_name,
                                                  $label) {
  $instance = array(
    'field_name' => $field_name,
    'entity_type' => 'taxonomy_term',
    'bundle' => $vocabulary_name,
    'label' => $label,
  );
  lol_import_field_add_instance($instance);
}

################################################################################
## TERM REFERENCE FIELD

/**
 * Create a field and field instance of a term reference field (if not exists).
 *
 * @param string $vocabulary_name machine readable name of the vocabulary
 * @param string $field_name machine readable name of the field
 * @param string $label field label
 * @param int $cardinality Number of values (-1 for unlimited)
 * @param string $referenced_vocabulary_name vicabulary the refence points too
 */
function lol_import_field_create_reference($vocabulary_name, $field_name,
                                           $label, $cardinality = -1,
                                           $referenced_vocabulary_name = NULL) {
  if ($referenced_vocabulary_name == NULL) {
    $referenced_vocabulary_name = $vocabulary_name;
  }
  lol_import_field_create_reference_field($field_name,
                                          $referenced_vocabulary_name,
                                          $cardinality);
  lol_import_field_create_reference_instance($vocabulary_name, $field_name,
                                             $label);
}

function lol_import_field_create_reference_field($field_name,
                                                 $referenced_vocabulary_name,
                                                 $cardinality = -1) {
  $field = array(
    'field_name' => $field_name,
    'type' => 'taxonomy_term_reference',
    'entity_type' => 'taxonomy_term',
    'cardinality' => $cardinality,
    'settings' => array(
      'allowed_values' => array(
        array(
          'vocabulary' => $referenced_vocabulary_name,
          'parent' => '0',
        ),
      ),
    ),
  );
  lol_import_field_add_field($field);
}

function lol_import_field_create_reference_instance($vocabulary_name,
                                                    $field_name, $label) {
  $instance = array(
    'field_name' => $field_name,
    'entity_type' => 'taxonomy_term',
    'bundle' => $vocabulary_name,
    'label' => $label,
    'widget' => array(
      'type' => 'taxonomy_autocomplete',
      'settings' => array(
        'autocomplete_path' => 'taxonomy/autocomplete',
      ),
      'module' => 'taxonomy',
    ),
  );
  lol_import_field_add_instance($instance);
}

################################################################################
## FIELD CREATION HELPER

/**
 * Add a field if not exists.
 *
 * @param array $field the field to be added
 */
function lol_import_field_add_field($field) {
  if (field_info_field($field['field_name'])) {
    // field exists
    return;
  }

  field_create_field($field);
}

/**
 * Add a field instance if not exists.
 *
 * @param array $instance the instance to be added
 */
function lol_import_field_add_instance($instance) {
  $instances = field_info_instances($instance['entity_type'],
                                    $instance['bundle']);
  foreach ($instances as $key => $value) {
    if ($instance['field_name'] == $key) {
      // instance exists
      return;
    }
  }

  field_create_instance($instance);
}